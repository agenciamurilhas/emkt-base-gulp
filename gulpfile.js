const { src, dest, watch, parallel } = require('gulp')
const sass   = require('gulp-sass')
const inline = require('gulp-inline-css')
const browserSync = require('browser-sync')
const plumber = require('gulp-plumber')
const inlineSource = require('gulp-inline-source')

function compileSass(){
  return src('./src/style.sass')
    .pipe(plumber())
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(dest('./src/'))
    .pipe(browserSync.reload({ stream: true }))
}

function inlineCSS() {
  return src('./src/*.html')
    .pipe(plumber())
    .pipe(inline({
      preserveMediaQueries: true,
      removeHtmlSelectors: false,
      applyStyleTags: false,
      removeStyleTags: false
    }))
    .pipe(dest('./dist'))
    .pipe(browserSync.reload({ stream: true }))
}

function inlineSourceCSS() {
  return src('./src/*.html')
    .pipe(plumber())
    .pipe(inlineSource())
    .pipe(dest('./dist'))
}

function imagesBuild(){
  return src('./src/images/**')
    .pipe(dest('./dist/images'))
}

function bs() {
  browserSync.init({
    server: {
      baseDir: './src',
      serveStaticOptions: {
        extensions: ['html'],
      },
    },
  });
  watch('./src/*.sass', compileSass).on('change', browserSync.reload)
  watch('./src/*.html', inlineCSS).on('change', browserSync.reload)
}

exports.bs = bs
exports.compileSass = compileSass
exports.inlineCSS = inlineCSS
exports.imagesBuild = imagesBuild
exports.inlineSource = inlineSourceCSS
exports.build = parallel(inlineCSS, imagesBuild)
exports.default = parallel(compileSass, bs)
